package project;
import java.util.*;

/**
 * @author Katie Butts
 * @author Annie Dima
 * @author Rachel Fischmar
 * CSE 374A
 * Dr. Raychoudhury
 * April 11th, 2022
 * Description: 
 */
public class Elevators {
	/**
	 * 
	 * @param level
	 * @param position
	 * @return
	 */
	public int minDistToRequestedLevel(int level, int[][] elevatorPositions) {
		int minDist = 0;
		if (groundLevel(elevatorPositions[level])) return minDist;
		int i = 1;
		int underneath = level - i;
		int above = level + i;
		while (onALevel(elevatorPositions.length, underneath) || onALevel(elevatorPositions.length, above)) {
			if (onALevel(elevatorPositions.length, underneath) && groundLevel(elevatorPositions[underneath])) {
				minDist = i;
				return minDist;
			}
			if (onALevel(elevatorPositions.length, above) && groundLevel(elevatorPositions[above])) {
				minDist = i;
				return minDist;
			}
			i++;
			underneath = level - i;
			above = level + i;
		}
		minDist = -1;
		return minDist;
	}
	
	/**
	 * 
	 * @param level
	 * @return
	 */
	private boolean groundLevel(int[] level) {
		boolean ground = Arrays.binarySearch(level, 1) > -1;
		return ground;
	}
	
	/**
	 * 
	 * @param numOfLevels
	 * @param level
	 * @return
	 */
	private boolean onALevel(int numOfLevels, int level) {
		if (level > -1 && level < numOfLevels) {
			return true;
		}
		return false;
	}
}
