package project;

public class StopWatch {

	private long totalTime, startTime, stopTime;
	private boolean isRunning;
	
	public StopWatch() {
		reset();
	}
	
	private void reset() {
		// totalTime = startTime = stopTime = 0;
		totalTime = 0;
		startTime = 0;
		stopTime = 0;
		isRunning = false;
	}
	
	//starts the timer 
	public void start() {
		if(isRunning) {
			return;
		} else {
			isRunning = true;
			startTime = System.nanoTime();
		}
	}
	
	//stops the timer
	public void stop() {
		if(!isRunning) {
			return;
		} else {
			stopTime = System.nanoTime();
			isRunning = false;
			totalTime += (stopTime - startTime);
		}
	}
	
	//return the total time from the stop watch 
	public long getTotalTime() {
		if(!isRunning) {
			return totalTime;
		}
		
		return totalTime + (System.nanoTime() - startTime);
		//return (totalTime + (System.nanoTime() - startTime)) / 1000000000;
	}
	
} //end class
