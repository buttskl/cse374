package project;

import static org.junit.Assert.*;

import org.junit.Test;

public class ElevatorsTest {
	
	StopWatch sw = new StopWatch();
	
	// test cases with one elevator, one person, and two floors
	
	@Test
	public void testGroundToFirst1() {
		int[][] elevators = {{1}, // ground floor (lobby)
							 {0}}; // first floor
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(1, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testFirstToGround1() {
		int[][] elevators = {{0}, // ground floor (lobby)
							 {1}}; // first floor
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(0, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	// test cases with one elevator, one person, and three floors
	
	@Test
	public void testGroundToFirst2() {
		int[][] elevators = {{1}, // ground floor (lobby)
							 {0},
							 {0}};
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(1, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testFirstToGround2() {
		int[][] elevators = {{0}, // ground floor (lobby)
							 {1},
							 {0}};
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(0, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testGroundToSecond1() {
		int[][] elevators = {{1}, // ground floor (lobby)
							 {0},
							 {0}};
		sw.start();
		assertEquals(2, new Elevators().minDistToRequestedLevel(2, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testSecondToGround1() {
		int[][] elevators = {{0}, // ground floor (lobby)
							 {0},
							 {1}};
		sw.start();
		assertEquals(2, new Elevators().minDistToRequestedLevel(0, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testFirstToSecond1() {
		int[][] elevators = {{0}, // ground floor (lobby)
							 {1},
							 {0}};
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(2, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testSecondToFirst1() {
		int[][] elevators = {{0}, // ground floor (lobby)
							 {0},
							 {1}};
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(1, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}

	// test cases with three elevators, one person, and three floors
	
	@Test
	public void test() {
		System.out.println("\ntest cases with 3 elevators \n");
		int[][] elevators = {{0, 1, 0}, // ground floor (lobby)
							 {1, 0, 0}, // first floor
							 {0, 0, 1}}; // third floor
		sw.start();
		assertEquals(0, new Elevators().minDistToRequestedLevel(2, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testThirdToGround() {
		int[][] elevators = {{0, 0, 0}, // ground floor (lobby)
							 {0, 0, 0}, // first floor
							 {1, 1, 1}}; // third floor
		sw.start();
		assertEquals(2, new Elevators().minDistToRequestedLevel(0, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testGroundToThird() {
		int[][] elevators = {{1, 1, 1}, 
							 {0, 0, 0}, 
							 {0, 0, 0}};
		sw.start();
		assertEquals(2, new Elevators().minDistToRequestedLevel(2, elevators));sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testThirdToFirst() {
		int[][] elevators = {{0, 0, 1}, 
							 {0, 1, 0}, 
							 {1, 0, 0}};
		sw.start();
		assertEquals(0, new Elevators().minDistToRequestedLevel(1, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
	@Test
	public void testFirstToThird() {
		int[][] elevators = {{0, 0, 0}, 
							 {1, 1, 1}, 
							 {0, 0, 0}};
		sw.start();
		assertEquals(1, new Elevators().minDistToRequestedLevel(2, elevators));
		sw.stop();
		System.out.println(sw.getTotalTime());
		sw = new StopWatch();
	}
	
} //end class
